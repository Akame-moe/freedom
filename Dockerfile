FROM alpine:latest

LABEL maintainer="everyone <love@freedom.com>" version="1.0" desp="this script written only for myself"
#modified by anonymous at 2020-06
#only extract  "v2ray" "v2ctl" "geoip.dat" "geosite.dat"  from zip archive into /usr/bin/v2ray/ directory
RUN set -ex \
    && apk --no-cache add --update tzdata ca-certificates curl unzip \
    && mkdir -p /tmp/v2ray \
    && mkdir -p /var/log/v2ray/ \
    && curl -L -H "Cache-Control: no-cache" -o /tmp/v2ray/v2ray.zip "https://cdn.jsdelivr.net/gh/v2ray/dist/v2ray-linux-64.zip" \
    && unzip -juo /tmp/v2ray/v2ray.zip "v2ray" "v2ctl" "geoip.dat" "geosite.dat" -d /usr/bin/v2ray/ \
    && chmod +x '/usr/bin/v2ray/v2ray' '/usr/bin/v2ray/v2ctl' \
    && rm -rf /tmp/v2ray


ENV TZ=Asia/Shanghai
VOLUME [ "/var/log/v2ray","/etc/v2ray" ]
EXPOSE 1081 1888
ENTRYPOINT [ "/usr/bin/v2ray/v2ray" ]
CMD ["-config=/etc/v2ray/config.json"]

#docker build -t v2ray .
#docker run --name v2ray-service -v /var/log/v2ray/:/var/log/v2ray/ -v /etc/v2ray/:/etc/v2ray/ -p 1081:1081 -p 1888:1888 -d v2ray
#docker [start|stop] v2ray-service